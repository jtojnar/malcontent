Overview of changes in malcontent 0.6.0
=======================================

* Add icon for `malcontent-control` (thanks Jakub Steiner) (#9)

* Redesign `malcontent-control` UI in response to design feedback (#11)

* Add `AccountInfo` interface for metadata on parent accounts (!26)

* Fix translation of the UI (!31)

* Bugs fixed:
 - #9 Add icon for malcontent-control
 - #11 User controls UI tweaks
 - !26 accounts-service: Add AccountInfo interface
 - !27 user-selector: Fix some const-to-non-const cast warnings
 - !29 po: Add some missing files to POTFILES.in
 - !30 Add Ukrainian translation
 - !31 build: Fix definition of PACKAGE_LOCALE_DIR
 - !32 Add Brazilian Portuguese translation
 - !33 po: Order LINGUAS alphabetically
 - !34 More small UI tweaks

* Translation updates:
 - Portuguese (Brazil)
 - Ukrainian


Overview of changes in malcontent 0.5.0
=======================================

* Add libmalcontent-ui library for parental controls widgets

* Add malcontent-control parental controls app

* Add initial support for session limits (but more needs to be done)

* Rename some of the commands for `malcontent-client` and rename some C APIs
  (but with compatibility defines)

* Bugs fixed:
 - #6 Align GLib dependency requirements
 - !16 docs: Improve documentation of "app-filter-changed" signal
 - !18 build: Port meson-make-symlink script to Python
 - !19 Add session limits support and PAM module
 - !20 Initial version of parental controls app
 - !21 build: Fix default value of pamlibdir
 - !22 Iterate on UI of parental controls app
 - !23 Split widgets into separate library
 - !24 Allow user controls to be used for not-yet-existing users


Overview of changes in malcontent 0.4.0
=======================================

* Implement `--quiet` in the `check` and `set` subcommands of
  `malcontent-client` to make it nicer to use from scripts (!13)

* Add support for filtering apps by content type (!15)

* Bugs fixed:
 - !2 tests: Use gdbus-codegen to drop hand-coded interface definitions
 - !12 libmalcontent: Add missing field initialisers to BUILDER_INIT
 - !13 Implement --quiet in the check and set subcommands of malcontent-client
 - !14 Improve README coverage of whole system design
 - !15 Add support to filter by content type


Overview of changes in malcontent 0.3.0
=======================================

* API change for getting app filters: use `mct_manager_get_app_filter()` now,
  rather than `mct_get_app_filter()` (#1, !6)

* Support signalling that a user’s app filter has changed using
  `MctManager::app-filter-changed` (#1)

* Add a `MCT_APP_FILTER_ERROR_DISABLED` error to distinguish between the app
  filter not being installed/supported, and temporarily not working (!9)

* Add a top-level header file: use `#include <libmalcontent/malcontent.h>`

* Bugs fixed:
 - #1 Emit a signal when a user’s parental controls change
 - !5 docs: Fix NEWS entry
 - !6 lib: Change allow_interactive_authorization bool to flags
 - !7 build: Post-release version bump
 - !8 docs: Expand README to be more informative
 - !9 libmalcontent: Add MCT_APP_FILTER_ERROR_DISABLED error
 - !11 libmalcontent: Add a top-level header file


Overview of changes in malcontent 0.2.0
=======================================

* Renamed project from eos-parental-controls to malcontent

* Bugs fixed:
 - !1 Rename project